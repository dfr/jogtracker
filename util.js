
function initDB(db) {
	console.log('initializing database tables..');
	db.run("CREATE TABLE if not exists user (id INTEGER PRIMARY KEY, email TEXT UNIQUE, name TEXT, role TEXT, password TEXT)");
	db.run("CREATE TABLE if not exists timeentry (id INTEGER PRIMARY KEY, user INTEGER, date INTEGER, distance INTEGER, time INTEGER)");
}

module.exports.initDB = initDB;

