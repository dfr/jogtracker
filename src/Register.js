import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Form, Input, Icon, Button } from 'antd';
const FormItem = Form.Item;

import history from './history';
import { inputChange, registerUser } from './reducer';
//import './Register.css';

const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 5 },
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 12 },
	},
};

class Register extends Component {

	static propTypes = {
		item: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired
	}

	onLoginClick = (e) => {
		history.push('/login');
	}

	onSubmit = (e) => {
		e.preventDefault();
		const { registerUser, item } = this.props;
		registerUser(item);
	}

	componentDidMount() {
	}

	render() {
		const { errors } = this.props;
		const { getFieldDecorator } = this.props.form;
		return (
			<Form onSubmit={this.onSubmit}>
				<FormItem
					{...formItemLayout}
					label="E-mail"
					validateStatus={errors.email ? 'error' : 'success'}
					help={errors.email}>
					{getFieldDecorator('email')(<Input />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Name"
					validateStatus={errors.name ? 'error' : 'success'}
					help={errors.name}>
					{getFieldDecorator('name')(<Input />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Passowrd"
					validateStatus={errors.password ? 'error' : 'success'}
					help={errors.password}>
					{getFieldDecorator('password')(<Input prefix={<Icon type="lock" style={{ fontSize: 13, marginLeft: 0 }} />} type="password" />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Repeat passowrd">
					{getFieldDecorator('passwordr')(<Input prefix={<Icon type="lock" style={{ fontSize: 13, marginLeft: 0 }} />} type="password" />)}
				</FormItem>

				<FormItem {...formItemLayout}>
					<Button type="primary" style={{ margin: 4 }} size="large" htmlType="submit">Register</Button>
					<Button type="primary" size="large" onClick={this.onLoginClick}>Login</Button>
				</FormItem>
				
			</Form>
		);
	}
}


const mapProps = (state) => {
 	return {
 		item: state.item,
 		errors: state.errors,
 	};
};

const mapDispatch = { inputChange, registerUser };
const formOpts = {
	onFieldsChange(props, changedFields) {
		props.inputChange(changedFields);
	},
	mapPropsToFields({ item }) {
		return item;
	}
};
export default connect(mapProps, mapDispatch)(Form.create(formOpts)(Register));

