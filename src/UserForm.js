import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Form, Input, Icon, Button, Radio } from 'antd';
const RadioGroup = Radio.Group;
const FormItem = Form.Item;

import history from './history';
import { inputChange, createUser, loadUsers } from './reducer';
//import './UserForm.css';

const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 5 },
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 12 },
	},
};

class UserForm extends Component {

	static propTypes = {
		item: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired,
		pathname: PropTypes.string.isRequired
	}

	onBackClick = (e) => {
		history.push('/user');
	}

	onSubmit = (e) => {
		e.preventDefault();
		const { createUser, item } = this.props;
		createUser(item);
	}

	componentDidMount() {
		const [_, query] = this.props.pathname.split('?');
		if(query && /id=\d/.test(query)) {
			const [_, id] = query.split('=');
			this.props.loadUsers({ id });
		}
	}

	render() {
		const { errors } = this.props;
		const { getFieldDecorator } = this.props.form;
		return (
			<Form onSubmit={this.onSubmit}>
				<FormItem
					{...formItemLayout}
					label="E-mail"
					validateStatus={errors.email ? 'error' : 'success'}
					help={errors.email}>
					{getFieldDecorator('email')(<Input />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Name"
					validateStatus={errors.name ? 'error' : 'success'}
					help={errors.name}>
					{getFieldDecorator('name')(<Input />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Role"
					validateStatus={errors.role ? 'error' : 'success'}
					help={errors.role}>
					{getFieldDecorator('role')(
						 <RadioGroup>
							 <Radio value={'user'}>User</Radio>
							 <Radio value={'manager'}>Manager</Radio>
							 <Radio value={'admin'}>Admin</Radio>
						 </RadioGroup>)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Passowrd"
					validateStatus={errors.password ? 'error' : 'success'}
					help={errors.password}>
					{getFieldDecorator('password')(<Input prefix={<Icon type="lock" style={{ fontSize: 13, marginLeft: 0 }} />} type="password" />)}
				</FormItem>

				<FormItem {...formItemLayout}>
					<Button type="primary" style={{ margin: 4 }} size="large" htmlType="submit">Submit</Button>
					<Button type="primary" size="large" onClick={this.onBackClick}>Cancel</Button>
				</FormItem>
				
			</Form>
		);
	}
}


const mapProps = (state) => {
 	return {
 		item: state.item,
 		errors: state.errors,
		pathname: state.pathname,
 		loading: state.loading
 	};
};

const mapDispatch = { inputChange, createUser, loadUsers };
const formOpts = {
	onFieldsChange(props, changedFields) {
		props.inputChange(changedFields);
	},
	mapPropsToFields({ item }) {
		return item;
	}
};
export default connect(mapProps, mapDispatch)(Form.create(formOpts)(UserForm));

