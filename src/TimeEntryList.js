import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Table, Button, Popconfirm, Form, DatePicker } from 'antd';
const FormItem = Form.Item;

import history from './history';
import { loadTimeEntries, deleteTimeEntry, inputChange } from './reducer';

class TimeEntryList extends Component {

	constructor(props) {
		super(props);
		this.dataColumns = [{
			title: 'Id',
			dataIndex: 'id',
			key: 'id'
		}, {
			title: 'Date',
			dataIndex: 'date',
			key: 'date',
			render: text => <span>{text.format('l')}</span>
		}, {
			title: 'Distance, km',
			dataIndex: 'distance',
			key: 'distance',
		}, {
			title: 'Time',
			dataIndex: 'time',
			key: 'time',
			render: text => <span>{text.format('HH:mm:ss')}</span>
		}, {
			title: 'Average speed, km/h',
			key: 'avspeed',
			render: (_ ,rec) => <span>{rec.time.unix() > 0 ? Math.round(rec.distance / (rec.time.unix() / 3600)) : 0}</span>
		} ];
		if(props.user._id && props.user.role === 'admin') this.dataColumns.push({
			title: 'Owner',
			key: 'owner',
			dataIndex: 'user'
		});
		this.dataColumns.push({
			title: 'Action',
			key: 'action',
			render: (text, record, index) => (
				<span>
					<Button icon="edit" style={{ marginRight: 14 }} onClick={() => history.push('/timeentry/edit?id='+record.id)}>Edit</Button>
					<Popconfirm title="Sure to delete?" onConfirm={() => this.onDeleteClick(index)}>
						<Button type="danger" icon="minus-circle">Remove</Button>
					</Popconfirm>
				</span>
			)
		});

	}

	static propTypes = {
		items: PropTypes.array.isRequired,
		user: PropTypes.object.isRequired,
		loading: PropTypes.bool.isRequired,
	}

	onAddClick = (e) => history.push('/timeentry/edit');

	onDeleteClick = (index) => {
		this.props.deleteTimeEntry(index);
	}

	componentDidMount() {
		this.props.loadTimeEntries(this.props.item);
	}

	componentWillReceiveProps(nextProps) {
		const [date_from0, date_from] = [this.props.item.date_from, nextProps.item.date_from].map(
			(p) => p && p.value ? p.value.hour(0).minute(0).second(0).unix() : '');
		const [date_to0, date_to] = [this.props.item.date_to, nextProps.item.date_to].map(
			(p) => p && p.value ? p.value.hour(23).minute(59).second(59).unix() : '');
		if(`${date_from0}_${date_to0}` !== `${date_from}_${date_to}`)
			this.props.loadTimeEntries({ date_from, date_to });
	}

	render() {
		const { getFieldDecorator } = this.props.form;
		const dataSource = this.props.items.map(it => ({ ...it, key: it.id }));
		
		return (
			<div>
				
				<Form layout="inline">
					<FormItem>
						<Button style={{ marginBottom: 8 }} type="primary" icon="plus" onClick={this.onAddClick}>Add new time entry</Button>
					</FormItem>

					<FormItem
						label="Search by date interval from:">
						{getFieldDecorator('date_from')(<DatePicker />)}
					</FormItem>

					<FormItem
						label="to">
						{getFieldDecorator('date_to')(<DatePicker />)}
					</FormItem>

				</Form>				
				<Table pagination={false} loading={this.props.loading} dataSource={dataSource} columns={this.dataColumns} />
			</div>
		);
	}
}


const mapProps = (state) => {
 	return {
 		item: state.item,
 		items: state.items,
 		user: state.user,
 		loading: state.loading
 	};
};

const mapDispatch = { loadTimeEntries, inputChange, deleteTimeEntry };

const formOpts = {
	onFieldsChange(props, changedFields) {
		props.inputChange(changedFields);
	},
	mapPropsToFields({ item }) {
		return item;
	}
};
export default connect(mapProps, mapDispatch)(Form.create(formOpts)(TimeEntryList));
