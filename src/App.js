import React, { Component } from 'react';
import PropTypes from 'prop-types';


import { Alert, Layout,  Menu, Icon } from 'antd';
const { Header, Content, Footer, Sider } = Layout;
import { connect } from 'react-redux';


import logo from './logo.svg';
import './App.css';

import TimeEntryList from './TimeEntryList';
import TimeEntryForm from './TimeEntryForm';
import WeeklyReport from './WeeklyReport';
import UserList from './UserList';
import UserForm from './UserForm';
import Register from './Register';
import LoginForm from './LoginForm';
import history from './history';
import { logoutUser } from './reducer';


const PAGES = {
    '/timeentry': TimeEntryList,
    '/timeentry/edit': TimeEntryForm,
    '/report': WeeklyReport,
    '/user': UserList,
    '/user/edit': UserForm,
    '/register': Register,
    '/login': LoginForm
};

class App extends Component {
	state = {
		collapsed: false,
		mode: 'inline',
	};
	onCollapse = (collapsed) => {
		console.log(collapsed);
		this.setState({
			collapsed,
			mode: collapsed ? 'vertical' : 'inline',
		});
	}

	onMenuClick = ({ key }) => {
		if(key !== 'logout') history.push(key);
		else this.props.logoutUser();
	}

	static propTypes = {
		pathname: PropTypes.string.isRequired,
		error: PropTypes.string.isRequired,
		user: PropTypes.object.isRequired
	};
	
	render() {
		const {error, user, pathname} = this.props;
		const [path] = pathname.split('?');

		const ChildComponent = PAGES[path] || (() => <div>Welcome to jogging tracker!</div>);

		return (
			<Layout>
				<Sider
					collapsible
					collapsed={this.state.collapsed}
					onCollapse={this.onCollapse}
				>
					<img src={logo} className="App-logo" alt="logo" />
					<Menu theme="dark" mode='inline' defaultSelectedKeys={[path]} onClick={this.onMenuClick}>
						<Menu.Item key="/timeentry">
							<span>
								<Icon type="file" />
								<span className="nav-text">Time Entries</span>
							</span>
						</Menu.Item>
						<Menu.Item key="/report">
							<span>
								<Icon type="file" />
								<span className="nav-text">Weekly report</span>
							</span>
						</Menu.Item>
						{user._id && (user.role === 'manager' || user.role === 'admin') && <Menu.Item key="/user">
							<span>
								<Icon type="file" />
								<span className="nav-text">Users</span>
							</span>
						</Menu.Item>}
						{!user._id && <Menu.Item key="/login">
							<span>
								<Icon type="file" />
								<span className="nav-text">Login</span>
							</span>
						</Menu.Item>}
						{user._id && <Menu.Item key="logout">
							<span>
								<Icon type="file" />
								<span className="nav-text">Logout</span>
							</span>
						</Menu.Item>}
					</Menu>
				</Sider>
				<Layout>
					<Header className="header">
						{'Time entries (' + (user._id ? `${user.email} [${user.role}]` : 'not logged in') + ')'}
					</Header>
					<Content style={{ margin: '0 16px' }}>
						<div style={{ margin: '12px 0' }}>
						</div>
						<div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
							{ error && <Alert
										   message="Error!"
										   description={error}
										   type="error"
										   showIcon
									   />}
							<ChildComponent />
						</div>
					</Content>
					<Footer style={{ textAlign: 'center' }}>
						App footer
					</Footer>
				</Layout>
			</Layout>
		);
	}
}

const mapProps = (state) => {
	return {
		pathname: state.pathname,
		user: state.user,
		error: state.errors.main ? state.errors.main : ''
	};
};

const mapDispatch = { logoutUser };
export default connect(mapProps, mapDispatch)(App);
