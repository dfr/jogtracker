import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Form, Input, Icon, Button } from 'antd';
const FormItem = Form.Item;

import history from './history';
import { inputChange, loginUser } from './reducer';
//import './LoginForm.css';

const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 5 },
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 12 },
	},
};

class LoginForm extends Component {

	static propTypes = {
		item: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired,
		loading: PropTypes.bool.isRequired,
		pathname: PropTypes.string.isRequired
	}

	onSubmit = (e) => {
		e.preventDefault();
		const { loginUser, item } = this.props;
		loginUser(item);
	}

	onRegisterClick = (e) => {
		e.preventDefault();
		history.push('/register');
	}

	componentDidMount() {
	}

	render() {
		const { errors } = this.props;
		const { getFieldDecorator } = this.props.form;
		return (
			<Form onSubmit={this.onSubmit}>
				<FormItem
					{...formItemLayout}
					label="E-mail"
					validateStatus={errors.username ? 'error' : 'success'}
					help={errors.username}>
					{getFieldDecorator('username')(<Input />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Passowrd"
					validateStatus={errors.password ? 'error' : 'success'}
					help={errors.password}>
					{getFieldDecorator('password')(<Input prefix={<Icon type="lock" style={{ fontSize: 13, marginLeft: 0 }} />} type="password" />)}
				</FormItem>

				<FormItem {...formItemLayout}>
					<Button type="primary" style={{ margin: 4 }} size="large" htmlType="submit">Login</Button>
					<Button type="primary" size="large" onClick={this.onRegisterClick}>Register</Button>
				</FormItem>
				
			</Form>
		);
	}
}


const mapProps = (state) => {
 	return {
 		item: state.item,
 		errors: state.errors,
		pathname: state.pathname,
 		loading: state.loading
 	};
};

const mapDispatch = { inputChange, loginUser };
const formOpts = {
	onFieldsChange(props, changedFields) {
		props.inputChange(changedFields);
	},
	mapPropsToFields({ item }) {
		return item;
	}
};
export default connect(mapProps, mapDispatch)(Form.create(formOpts)(LoginForm));

