import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Table, Button, Popconfirm } from 'antd';

import history from './history';
import { loadUsers, deleteUser } from './reducer';
//import './UserList.css';

class UserList extends Component {

	constructor(props) {
		super(props);
		this.dataColumns = [{
			title: 'Id',
			dataIndex: 'id',
			key: 'id'
		}, {
			title: 'E-mail',
			dataIndex: 'email',
			key: 'email',
			render: text => <span>{text}</span>
		}, {
			title: 'Name',
			dataIndex: 'name',
			key: 'name',
		}, {
			title: 'Role',
			dataIndex: 'role',
			key: 'role',
		}, {
			title: 'Action',
			key: 'action',
			render: (text, record, index) => (
				<span>
					<Button icon="edit" style={{ marginRight: 14 }} onClick={() => history.push('/user/edit?id='+record.id)}>Edit</Button>
					<Popconfirm title="Sure to delete?" onConfirm={() => this.onDeleteClick(index)}>
						<Button type="danger" icon="minus-circle">Remove</Button>
					</Popconfirm>
				</span>
			),
		}];
	}

	static propTypes = {
		items: PropTypes.array.isRequired,
		loading: PropTypes.bool.isRequired,
	}

	onAddClick = (e) => history.push('/user/edit');

	onDeleteClick = (index) => {
		this.props.deleteUser(index);
	}

	componentDidMount() {
		this.props.loadUsers();
	}

	render() {

		const dataSource = this.props.items.map(it => ({ ...it, key: it.id }));
		
		return (
			<div>
				<Button style={{ marginBottom: 8 }} type="primary" icon="plus" onClick={this.onAddClick}>Add new user</Button>
				<Table pagination={false} loading={this.props.loading} dataSource={dataSource} columns={this.dataColumns} />
			</div>
		);
	}
}


const mapProps = (state) => {
 	return {
 		items: state.items,
 		loading: state.loading
 	};
};

const mapDispatch = { loadUsers, deleteUser };
export default connect(mapProps, mapDispatch)(UserList);

