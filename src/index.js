import React from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'universal-cookie';
import {Provider} from 'react-redux';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import App from './App';
import history from './history';
import store from './store';
import { AUTH_USER } from './reducer.js';
import './index.css';

// update location state
history.push(location.pathname);

// update auth state
const cookies = new Cookies();
const token = cookies.get('token');
const user = cookies.get('user');
if (token) {
	store.dispatch({ type: AUTH_USER, user });
}


ReactDOM.render(
	<LocaleProvider locale={enUS}>
		<Provider store={store}>
			<App pathname={location.pathname} />
		</Provider>
	</LocaleProvider>,
	document.getElementById('root')
);
