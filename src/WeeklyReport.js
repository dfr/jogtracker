import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Table } from 'antd';

import { loadTimeEntries } from './reducer';

class WeeklyReport extends Component {

	constructor(props) {
		super(props);
		this.dataColumns = [{
			title: 'Week',
			dataIndex: 'w',
			key: 'w',
			render: (w) => <span>{w}</span>
		}, {
			title: 'Average speed, km/h',
			dataIndex: 'speed',
			key: 'speed'
		}, {
			title: 'Average distance, km',
			dataIndex: 'distance',
			key: 'distance'
		}];

	}

	static propTypes = {
		items: PropTypes.array.isRequired,
		loading: PropTypes.bool.isRequired,
	}

	onAddClick = (e) => history.push('/timeentry/edit');

	onDeleteClick = (index) => {
		//console.log('del', index, this.props.items[index]);
		this.props.deleteTimeEntry(index);
	}

	componentDidMount() {
		this.props.loadTimeEntries();
	}

	render() {
		const {items} = this.props;
		let byWeek = {};
		for(var i = 0; i < items.length; i++) {
			const w = items[i].date.week() + ' of ' + items[i].date.year();
			const speed = items[i].time.unix() > 0 ? Math.round(items[i].distance / (items[i].time.unix() / 3600)) : 0;
			if(!(w in byWeek)) byWeek[w] = { speed: 0, distance: 0, count : 0 };
			byWeek[w].speed += speed;
			byWeek[w].distance += items[i].distance;
			byWeek[w].count++;
		}
		const dataSource = [];
		for(const w in byWeek) {
			const speed = Math.round(byWeek[w].speed / (byWeek[w].count || 1));
			const distance = Math.round(byWeek[w].distance / (byWeek[w].count || 1));
			dataSource.push({ w, speed, distance, key: w });
		}
		dataSource.sort((a, b) => a.w - b.w);
		
		return (
			<div>
				<Table pagination={false} loading={this.props.loading} dataSource={dataSource} columns={this.dataColumns} />
			</div>
		);
	}
}


const mapProps = (state) => {
 	return {
 		items: state.items,
 		loading: state.loading
 	};
};

const mapDispatch = { loadTimeEntries };
export default connect(mapProps, mapDispatch)(WeeklyReport);

