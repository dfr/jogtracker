import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';

import { Form, InputNumber, DatePicker, TimePicker, Button } from 'antd';
const FormItem = Form.Item;

import history from './history';
import { inputChange, createTimeEntry, loadTimeEntries } from './reducer';
//import './TimeEntryForm.css';

const formItemLayout = {
	labelCol: {
		xs: { span: 24 },
		sm: { span: 5 },
	},
	wrapperCol: {
		xs: { span: 24 },
		sm: { span: 12 },
	},
};

class TimeEntryForm extends Component {

	static propTypes = {
		item: PropTypes.object.isRequired,
		errors: PropTypes.object.isRequired,
		loading: PropTypes.bool.isRequired,
	}

	onBackClick = (e) => {
		history.push('/timeentry');
	}

	onSubmit = (e) => {
		e.preventDefault();
		const { createTimeEntry, item } = this.props;
		createTimeEntry(item);
	}

	componentDidMount() {
		const { setFieldsValue } = this.props.form;
		const [_, query] = this.props.pathname.split('?');
		if(query && /id=\d/.test(query)) {
			const [_, id] = query.split('=');
			this.props.loadTimeEntries({ id });
		}
		else {
			setFieldsValue({ date: moment() });
		}
	}

	render() {
		const { errors } = this.props;
		const { getFieldDecorator } = this.props.form;
		return (
			<Form onSubmit={this.onSubmit}>
				<FormItem
					{...formItemLayout}
					label="Date"
					validateStatus={errors.date ? 'error' : 'success'}
					help={errors.date}>
					{getFieldDecorator('date')(<DatePicker />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Distance"
					validateStatus={errors.distance ? 'error' : 'success'}
					help={errors.distance}>
					{getFieldDecorator('distance')(<InputNumber placeholder="Kilometers" />)}
				</FormItem>

				<FormItem
					{...formItemLayout}
					label="Time"
					validateStatus={errors.time ? 'error' : 'success'}
					help={errors.time}>
					{getFieldDecorator('time')(<TimePicker />)}
				</FormItem>

				<FormItem {...formItemLayout}>
					<Button type="primary" style={{ margin: 4 }} size="large" htmlType="submit">Submit</Button>
					<Button type="primary" size="large" onClick={this.onBackClick}>Cancel</Button>
				</FormItem>
				
			</Form>
		);
	}
}


const mapProps = (state) => {
 	return {
 		item: state.item,
 		errors: state.errors,
		pathname: state.pathname,
 		loading: state.loading
 	};
};

const mapDispatch = { inputChange, createTimeEntry, loadTimeEntries };
const formOpts = {
	onFieldsChange(props, changedFields) {
		props.inputChange(changedFields);
	},
	mapPropsToFields({ item }) {
		return item;
	}
};
export default connect(mapProps, mapDispatch)(Form.create(formOpts)(TimeEntryForm));

