
import {pathnameChange} from './reducer';
import store from './store';

const onChangeListeners = [
	(p) => store.dispatch(pathnameChange(p))
];

function push(pathname) {
    window.history.pushState({}, '', pathname);
    onChangeListeners.forEach(callback => callback(pathname));
}

function back() {
    window.history.back();
    onChangeListeners.forEach(callback => callback(window.location.href));
}

export default {
    push,
	back,
    onChange: cb => onChangeListeners.push(cb),
};
