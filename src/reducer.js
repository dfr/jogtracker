import moment from 'moment';
import 'whatwg-fetch';
import Cookies from 'universal-cookie';
import { message } from 'antd';

import history from './history';

const PATHNAME_CHANGED = 'PATHNAME_CHANGED';
const LOADING = 'LOADING';
const LOADED  = 'LOADED';
const MESSAGE  = 'MESSAGE';
const INPUT_CHANGED = 'INPUT_CHANGED';
const DELETE_ITEM = 'DELETE_ITEM';

export const AUTH_USER = 'AUTH_USER';
const UNAUTH_USER = 'UNAUTH_USER';

const cookies = new Cookies();

// small postprocessing for compability with ant form data representation
function postProcessForUIForm(sitem) {
	let item = {};
	if(sitem) {
		for(const k in sitem) item[k] = { name: k, value: sitem[k] };
	}
	return item;
}

function paramsToQueryString(params) {
	let query = Object.keys(params || {})
		.map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&');
	if(query) query = '?'+query;
	return query;
}

// Auth actions
export function loginUser(item) {
	return function(dispatch) {
		const data = {
			username: item.username ? item.username.value : '',
			password: item.password ? item.password.value : '' };
		dispatch({ type: LOADING });
		const req = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(data)
		};
		fetch('/login', req).then(resp => resp.json()).then((data) => {
			cookies.set('token', data.token, { path: '/' });
			cookies.set('user', data.user, { path: '/' });
			dispatch({ type: LOADED, data: { errors: {}, user: data.user } });
			history.push('/timeentry');
		}).catch(e => {
			console.error(e);
			dispatch({ type: LOADED, data: { errors: { main: 'Problem while logging in' } } });
		});
    }
}

export function logoutUser() {  
	return function (dispatch) {
		cookies.remove('token', { path: '/' });
		cookies.remove('user', { path: '/' });
		dispatch({ type: UNAUTH_USER });
		history.push('/login');
	}
}

// Common actions
function deleteItem(dispatch, getState, url, index) {
	const { items } = getState();
	const id = items[index].id;
	dispatch({ type: LOADING });
	const req = {
		method: 'DELETE',
		headers: { 'Content-Type': 'application/json', 'Authorization': cookies.get('token') || '' }
	};
	fetch(url + '/' + id, req).then(handleServerResponse).then((data) => {
		if('errors' in data) {
			dispatch({ type: LOADED, data });
		}
		else {
			dispatch({ type: LOADED, data: { errors: {} } });
			dispatch({ type: DELETE_ITEM, index });
		}
	}).catch(e => {
		console.error(e);
		dispatch({ type: LOADED, data: { errors: { main: 'Problem deleting item' } } });
	});
}

function addOrUpdate(dispatch, url, data, id) {
	dispatch({ type: LOADING });
	const req = {
		method: id ? 'PUT' : 'POST',
		headers: { 'Content-Type': 'application/json', 'Authorization': cookies.get('token') || '' },
		body: JSON.stringify(data)
	};
	return fetch(url + (id ? '/'+id : ''), req)
		.then(handleServerResponse).then((data) => {
			if('errors' in data) {
				dispatch({ type: LOADED, data });
				return false;
			}
			else {
				dispatch({ type: LOADED, data: { errors: {} } });
				return true;
			}
		}).catch(e => {
			console.error(e);
			dispatch({ type: LOADED, data: { errors: { main: 'Problem in saving: '+url } } });
			return false;
		});
}

export function inputChange(value){
	return {
		type: INPUT_CHANGED,
		value
	}
}

export function pathnameChange(pathname) {
	return { type: PATHNAME_CHANGED, pathname }
}


// Time entry actions
export function deleteTimeEntry(index) {
	return (dispatch, getState) => deleteItem(dispatch, getState, '/timeentry', index);
}

function handleServerResponse(resp) {
	if(resp.status === 401) {
		history.push('/login');
		return { authenticated: false , errors: { main: 'Access denied' } }
	}
	else
		return resp.json();
}

export function loadTimeEntries(params) {
	return dispatch => {
		dispatch({ type: LOADING });
		const req = {
			headers: { 'Authorization': cookies.get('token') || '' }
		};

		fetch('/timeentry'+paramsToQueryString(params), req).then(handleServerResponse).then((data) => {
			if(data.items) {
				const items = data.items.map(it => ({ ...it, date: moment.unix(it.date), time: moment.unix(it.time).utc() }));
				if(params && params.id) {
					const item = postProcessForUIForm(items[0])
					dispatch({ type: LOADED, data: { item } });
				}
				else {
					dispatch({ type: LOADED, data: { items } });
				}
			}
			else
				dispatch({ type: LOADED, data });
		}).catch(e => {
			console.error(e);
			dispatch({ type: LOADED, data: { errors: { main: 'Unable to load time entries' } } });
		});
	}
}

export function createTimeEntry(formData) {
	return dispatch => {
		const timeSeconds = formData.time ?
			formData.time.value.hours()*3600 + formData.time.value.minutes()*60 + formData.time.value.seconds() : '';
		const data = {
			date: formData.date ? formData.date.value.unix() : '',
			distance: formData.distance ? formData.distance.value : '',
			time: timeSeconds
		};
		addOrUpdate(dispatch, '/timeentry', data, formData.id ? formData.id.value : null)
			.then((ok) => {
				if(ok) history.push('/timeentry')
			});
	}
}

// User actions
export function deleteUser(index) {
	return (dispatch, getState) => deleteItem(dispatch, getState, '/user', index);
}

export function loadUsers(params) {
	return dispatch => {
		dispatch({ type: LOADING });
		const req = {
			headers: { 'Authorization': cookies.get('token') || '' }
		};
		fetch('/user'+paramsToQueryString(params), req).then(handleServerResponse).then((data) => {
			if(data.items) {
				if(params && params.id) {
					const item = postProcessForUIForm(data.items[0])
					dispatch({ type: LOADED, data: { item } });
				}
				else {
					dispatch({ type: LOADED, data: { items: data.items } });
				}
			}
			else
				dispatch({ type: LOADED, data });
		}).catch(e => {
			console.error(e);
			dispatch({ type: LOADED, data: { errors: { main: 'Unable to load users' } } });
		});
	}
}

export function createUser(formData) {
	return dispatch => {
		const data = {
			name: formData.name ? formData.name.value : '',
			email: formData.email ? formData.email.value : '',
			role: formData.role ? formData.role.value : '',
			password: formData.password ? formData.password.value : ''
		};
		addOrUpdate(dispatch, '/user', data, formData.id ? formData.id.value : null)
			.then((ok) => {
				if(ok) history.push('/user')
			});
	}
}

export function registerUser(formData) {
	return dispatch => {
		const data = {
			name: formData.name ? formData.name.value : '',
			email: formData.email ? formData.email.value : '',
			password: formData.password ? formData.password.value : '',
			passwordr: formData.passwordr ? formData.passwordr.value : ''
		};
		addOrUpdate(dispatch, '/register', data, null).then((ok) => {
			if(ok) {
				history.push('/login');
				message.info('Registration successful! Please login with your credentials.');
			}
		});
	}
}

const initialState = {
	items: [],
	item: {},
	errors: {},
	pathname: '',
	message: '',
	user: {},
	loading: false
};

export default function reducer(state = initialState, action){
	switch (action.type) {
		case AUTH_USER:
			return { ...state, user: action.user };
		case UNAUTH_USER:
			return { ...state, user: {} };
		case PATHNAME_CHANGED:
			if(state.pathname !== action.pathname)
				return { ...initialState, user: state.user, pathname: action.pathname };
			else
				return state;
		case INPUT_CHANGED:
			return Object.assign({}, state,
				{ item: { ...state.item, ...action.value } }
			);
		case MESSAGE:
			return { ...state, message: action.message };
		case LOADING:
			return { ...state, loading: true };
		case LOADED:
			if(action.data && action.data.message) message.error(action.data.message, 5);
			return Object.assign({}, state, { loading: false },	action.data || {});
		case DELETE_ITEM:
			return Object.assign(
				{},
				state, { items: [
						...state.items.slice(0, action.index),
						...state.items.slice(action.index+1)
				]}
			);
		default:
			return state;
	}
}

