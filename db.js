const sqlite3 = require('sqlite3').verbose();
const DB_FILE = 'test.db';
module.exports = new sqlite3.Database(process.env.NODE_ENV === 'test' ? ':memory:' : DB_FILE);

