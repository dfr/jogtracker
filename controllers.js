const jwt = require('jsonwebtoken');
const validator = require('validator');
const db = require('./db');
const config = require('./config');

const LIST_SIZE = 10;


function validateUser(email, name) {
	var errs = {};
	if(!email) errs.email = 'required';
	if(!name) errs.name = 'required';
	if(email && !validator.isEmail(String(email))) errs.email = 'invalid';
	if(name && name.length > 64) errs.name = 'too long';
	return errs;
}

function validateTimeEntry(te) {
	var errs = {};
	if(!te.date) errs.date = 'required';
	if(!te.distance) errs.distance = 'required';
	if(!te.time) errs.time = 'required';
	if(te.date && !validator.isInt(String(te.date))) errs.date = 'incorrect';
	if(te.distance && !validator.isInt(String(te.distance))) errs.distance = 'incorrect';
	if(te.time && !validator.isInt(String(te.time))) errs.time = 'incorrect';
	return errs;
}

function selectAllItemsFrom(table, req, res) {
	let where = [];
	let binds = [];
	if(req.query.id) {
		where.push('id=?');
		binds.push(req.query.id);
	}
	if(table === 'timeentry') {
		if(req.user.role !== 'admin') {
			where.push('user=?');
			binds.push(req.user.id);
		}
		if(req.query.date_from) {
			where.push('date >= ?');
			binds.push(req.query.date_from);
		}
		if(req.query.date_to) {
			where.push('date <= ?');
			binds.push(req.query.date_to);
		}
	}
	if(where.length > 0) where = ' WHERE '+ where.join(' AND ');

	const offset = (req.query.offset && validator.isInt(req.query.offset)) ? req.query.offset : 0;
	binds.push(LIST_SIZE);
	binds.push(offset);

	db.all(`SELECT * FROM ${table} ${where} LIMIT ? OFFSET ?`, binds, function (err, rows) {
		if(err) {
			console.error(err);
			return res.status(500).json({ message: err.code });
		}
		res.status(200).json({ items: rows });
	});
}

function timeEntryFromReq(req) {
	return {
		id: req.body.id,
		date: req.body.date,
		distance: req.body.distance,
		time: req.body.time
	};
}

function userTokenInfo(req) {
	return {
		_id: req.id,
		name: req.name,
		email: req.email,
		role: req.role
	};
}

function generateToken(user) {
	return jwt.sign(user, config.jwtOptions.secretOrKey, {
		expiresIn: 36000 // in seconds
	});
}

module.exports = {
	auth: {
		login: (req, res) => {
			let userInfo = userTokenInfo(req.user);

			res.status(200).json({
				token: 'JWT ' + generateToken(userInfo),
				user: userInfo
			});			
		},
		logout: (req, res) => {
			req.logout();
			res.redirect('/');
		},
		protected: (req, res) => {
			res.status(200).json({});
		},
		register: (req, res) => {
			db.get("SELECT * FROM user WHERE email = ?", [req.body.email], function (err, row) {
				if(row) {
					return res.status(409).json({ errors: { main: 'Account with this email address already exists!' } });
				}
				const sql = "INSERT INTO user (email, name, role, password) VALUES (?,?,?,?)";
				const errs = validateUser(req.body.email, req.body.name);
				if(!req.body.password) errs.password = 'required';
				if(req.body.password !== req.body.passwordr) errs.password = "doesn't match";
				if(Object.keys(errs).length > 0) return res.status(400).json({ errors: errs });
				
				const pwd = req.body.password; // TODO encrypt
				db.run(sql, [req.body.email, req.body.name, 'user', pwd], function(err) {
					if(err) return res.status(500).json({ message: 'Error occured during user creation: '+ err.code });
					res.status(200).json({ user: { id: this.lastID } });
				});
			});
		},
		
	},
	timeentry: {
		all: (req, res) => {
			selectAllItemsFrom('timeentry', req, res);
		},
		add: (req, res) => {
			const sql = "INSERT INTO timeentry (user, date, distance, time) VALUES (?,?,?,?)";
			var te = timeEntryFromReq(req);
			const errs = validateTimeEntry(te);
			if(Object.keys(errs).length > 0) return res.status(400).json({ errors: errs });
			db.run(sql, [req.user.id, te.date, te.distance, te.time], function(err) {
				if(err) return res.status(500).json({ message: 'Error occured during time entry creation: '+ err });
				res.status(200).json({ timeentry: { id: this.lastID } });
			});
		},
		update: (req, res) => {
			const sql = "UPDATE timeentry SET date=?, distance=?, time=? WHERE id=? AND user=?";
			var te = timeEntryFromReq(req);
			const errs = validateTimeEntry(te)
			if(!validator.isInt(req.params.id || '', { min: 0 })) errs.push({ error: 'id is incorrect' });
			if(Object.keys(errs).length > 0) return res.status(400).json({ errors: errs });
			db.run(sql, [te.date, te.distance, te.time, req.params.id, req.user.id], function(err) {
				if(err) return res.status(500).json({ message: 'Error occured during time entry update: '+ err.code });
				res.status(200).json({ timeentry: { id: req.params.id } });
			});
		},
		remove: (req, res) => {
			if(req.user.role === 'admin')
				db.run("DELETE FROM timeentry WHERE id = ?", [req.params.id]);
			else
				db.run("DELETE FROM timeentry WHERE id = ? AND user=?", [req.params.id, req.user.id]);
			res.status(200).json({ message: 'Ok' });
		},
	},
	user: {
		all: (req, res) => {
			// authorization check
			if(req.user) {
				if(req.user.role === 'user')
					return res.status(400).json({ errors: { main: 'Not enough access rights to list users' } });
			}
			selectAllItemsFrom('user', req, res);
		},
		add: (req, res) => {
			db.get("SELECT * FROM user WHERE email = ?", [req.body.email], function (err, row) {
				if(row) {
					return res.status(409).json({ errors: { email: 'Account with this email address already exists' } });
				}
				const sql = "INSERT INTO user (email, name, role, password) VALUES (?,?,?,?)";
				const errs = validateUser(req.body.email, req.body.name);
				if(!req.body.password) errs.password = 'required';
				if(!validator.isIn(String(req.body.role), ['user','admin','manager'])) errs.role = 'incorrect';
				// authorization checks
				if(req.user) {
					if(req.user.role === 'user') errs.main = 'Not enough access rights to add new user';
					else if(req.user.role === 'manager' && req.body.role === 'admin')
						errs.main = 'Manager is not allowed to add admin users';
				}
				if(Object.keys(errs).length > 0) return res.status(400).json({ errors: errs });
				
				const pwd = req.body.password; // TODO encrypt
				db.run(sql, [req.body.email, req.body.name, req.body.role, pwd], function(err) {
					if(err) return res.status(500).json({ errors: { main: 'Error occured during user creation: '+ err.code } });
					res.status(200).json({ user: { id: this.lastID } });
				});
			});
		},
		update: (req, res) => {
			const id = req.params.id;
			db.get("SELECT * FROM user WHERE id = ?", [id], function (err, row) {
				if(!row) {
					return res.status(409).json({ errors: { main: 'User with this id gone!' } });
				}
				const pwd = req.body.password; // TODO encrypt
				const errs = validateUser(req.body.email, req.body.name);
				if(!validator.isInt(String(id), { min: 0 })) errs.id = 'incorrect';
				if(!validator.isIn(String(req.body.role), ['user','admin','manager'])) errs.role = 'incorrect';
				// authorization checks
				if(req.user) {
					if(req.user.role === 'user') errs.main = 'Not enough access rights to update users';
					else if(req.user.role === 'manager' && req.body.role === 'admin')
						errs.main = 'Manager is not allowed to set admin rights';
					else if(req.user.role === 'manager' && row.role === 'admin')
						errs.main = 'Manager is not allowed to alter admin users';
				}
				if(Object.keys(errs).length > 0) return res.status(400).json({ errors: errs });
				let binds = [];
				let extra_set = '';
				// if password is not specified then just not update it
				if(req.body.password) {
					binds = [req.body.email, req.body.name, req.body.role, pwd, id]
					extra_set = ', password=?';
				}
				else {
					binds = [req.body.email, req.body.name, req.body.role, id]
				}
				db.run(`UPDATE user SET email=?, name=?, role =? ${extra_set} WHERE id = ?`, binds);
				res.status(200).json({ user: { id: id } });
			});
		},
		remove: (req, res) => {
			db.get("SELECT * FROM user WHERE id = ?", [req.params.id], function (err, row) {
				if(!row) return res.status(200).json({ message: 'Ok' });
				// authorization checks
				if(req.user) {
					if(req.user.role === 'user')
						return res.status(400).json({ errors: { main: 'Not enough access rights to delete users' } });
					if(req.user.role === 'manager' && row.role === 'admin')
						return res.status(400).json({ errors: { main: 'Not enough access rights to delete admin users' } });
				}
				db.run("DELETE FROM user WHERE id = ?", [req.params.id]);
				res.status(200).json({ message: 'Ok' });
			});
		},
	}
}
