const ExtractJwt = require('passport-jwt').ExtractJwt;

// JWT login strategy
const jwtOptions = {
	// Telling Passport to check authorization headers for JWT
	jwtFromRequest: ExtractJwt.fromAuthHeader(),
	// TODO
	secretOrKey: 'asdfasdfsdfsdfsdsa2#@#$!@#!#!$@#$#ZCZXCzxczXCZXCZXC'
};

module.exports = { jwtOptions: jwtOptions };
