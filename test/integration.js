process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const util = require('../util');

const server = require('../server');
const db = server.db;
const app = server.app;

let token = null;
//const app = 'http://localhost:3001';
//const db = require('../db');
//util.initDB(db);


chai.use(chaiHttp);

function doLogin(done) {
	db.run("DELETE FROM user", function() {
		db.run("INSERT INTO user (id, email, name, role, password) VALUES (1, ?,?,?,?)", ['test@test.com', 'John Doe', 'admin', 'test'], function(err, row) {
			chai.request(app)
				.post('/login')
				.set('Authorization', token)
				.send({ username: 'test@test.com', password: 'test' })
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.have.header('content-type', /application\/json/)
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('token');
					token = res.body.token;
					done();
				});
		});
	});
}


describe('User', () => {
	before(function(done) {
		doLogin(done);
	});

    beforeEach((done) => { //Before each test we clear the database
		db.run("DELETE FROM user WHERE id > 1", done);
    });

	describe('GET /user 1', () => {
		it('it should GET all the users', (done) => {
			chai.request(app)
				.get('/user')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.have.header('content-type', /application\/json/)
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(1);
					done();
				});
		});
	});
	describe('GET /user 2', () => {
		beforeEach((done) => {
			db.run("INSERT INTO user (id, email, name, password) VALUES (2, ?,?,?)", ['test1@test.com', 'John Doe', 'test'],  done);
		});
		it('it should GET all the users', (done) => {
			chai.request(app)
				.get('/user')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.have.header('content-type', /application\/json/)
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(2);
					res.body.items[1].id.should.be.eql(2);
					done();
				});
		});
	});

	describe('GET /user', () => {
		beforeEach((done) => {
			db.run("INSERT INTO user (id, email, name, password) VALUES (2, ?,?,?)", ['tes1t@test.com', 'John Doe1', 'test'],  function() {
				db.run("INSERT INTO user (id, email, name, password) VALUES (3, ?,?,?)", ['test2@test.com', 'John Doe2', 'test'],  done);
			});
		});
		it('it should GET all the users', (done) => {
			chai.request(app)
				.get('/user')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(3);
					res.body.items[1].id.should.be.eql(2);
					done();
				});
		});
		it('it should GET one user by ?id=..', (done) => {
			chai.request(app)
				.get('/user?id=3')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(1);
					res.body.items[0].id.should.be.eql(3);
					done();
				});
		});
		it('it should return no users on wrong id ?id=2000', (done) => {
			chai.request(app)
				.get('/user?id=2000')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('POST /user', () => {
		it('it should add new user', (done) => {
			const user = {
				email: 'test1@test.com',
				name: 'John Doe',
				role: 'user',
				password: 'test'
			};
			chai.request(app)
				.post('/user')
				.set('Authorization', token)
				.send(user)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('user');
					res.body.user.should.have.property('id');
					res.body.user.id.should.be.above(0);
					done();
				});
		});
		it('it should return errors for new invalid user', (done) => {
			const user = {
				email: 'testtest.com',
				role: 'nonexistent',
			};
			chai.request(app)
				.post('/user')
				.set('Authorization', token)
				.send(user)
				.end((err, res) => {
					res.should.have.status(400);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('errors');
					res.body.errors.should.be.a('object');
					res.body.errors.should.have.property('name');
					res.body.errors.should.have.property('email');
					res.body.errors.should.have.property('role');
					done();
				});
		});
	});

	describe('PUT /user', () => {
		beforeEach((done) => { //Before each test we empty the database
			db.run("INSERT INTO user (id, email, name, password) VALUES (2,?,?,?)", ['test1@test.com', 'John Doe', 'test'],  done);
		});
		it('it should update user', (done) => {
			const user = {
				email: 'test1@test.com',
				name: 'Richard Doe',
				role: 'user',
				password: 'test123'
			};
			chai.request(app)
				.put('/user/2')
				.set('Authorization', token)
				.send(user)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('user');
					res.body.user.should.have.property('id');
					res.body.user.id.should.be.above(0);
					done();
				});
		});
		it('it should return errors for updating with invalid email, empty pwd and missing name', (done) => {
			const user = {
				email: 'test1test.com',
				password: ''
			};
			chai.request(app)
				.put('/user/2')
				.set('Authorization', token)
				.send(user)
				.end((err, res) => {
					res.should.have.status(400);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('errors');
					res.body.errors.should.be.a('object');
					res.body.errors.should.have.property('name');
					res.body.errors.should.have.property('email');
					done();
				});
		});
	});

	describe('DELETE /user', () => {
		beforeEach((done) => { //Before each test we empty the database
			db.run("INSERT INTO user (id, email, name, password) VALUES (2,?,?,?)", ['test1@test.com', 'John Doe', 'test'],  done);
		});
		it('it should delete user', (done) => {
			chai.request(app)
				.delete('/user/2')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('message');
					done();
				});
		});
	});

});

describe('Time Entry', () => {
	before(function(done) {
		doLogin(done);
	});

	function insTimeEntry(id, cb) {
		db.run("INSERT INTO timeentry (id, user, date, distance, time) VALUES (?,?,?,?,?)",
			[id, 1, 2342433, 200, 600], cb);
	}
	
    beforeEach((done) => { //Before each test we empty the database
		db.run("DELETE FROM timeentry", done);
    });

	describe('GET /timeentry 0', () => {
		it('it should GET all the timeentries', (done) => {
			chai.request(app)
				.get('/timeentry')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.have.header('content-type', /application\/json/)
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('GET /timeentry', () => {
		beforeEach((done) => {
			insTimeEntry(1, function() {
				insTimeEntry(2, done)
			});
		});
		it('it should GET all the timeentries', (done) => {
			chai.request(app)
				.get('/timeentry')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.have.header('content-type', /application\/json/)
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(2);
					res.body.items[0].id.should.be.eql(1);
					res.body.items[0].user.should.be.eql(1);
					res.body.items[0].time.should.be.eql(600);
					done();
				});
		});
		it('it should GET timeentry by id', (done) => {
			chai.request(app)
				.get('/timeentry?id=2')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(1);
					res.body.items[0].id.should.be.eql(2);
					done();
				});
		});
		it('it should GET no timeentries by wrong id', (done) => {
			chai.request(app)
				.get('/timeentry?id=2222')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(0);
					done();
				});
		});
		it('it should GET no timeentries by malformed id', (done) => {
			chai.request(app)
				.get("/timeentry?id='1=1'''")
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('items');
					res.body.items.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('POST /timeentry', () => {
		it('it should add new timeentry', (done) => {
			const timeentry = {
				user: '1',
				date: '123123123',
				distance: '200',
				time: '600'
			};
			chai.request(app)
				.post('/timeentry')
				.set('Authorization', token)
				.send(timeentry)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('timeentry');
					res.body.timeentry.should.have.property('id');
					res.body.timeentry.id.should.be.above(0);
					done();
				});
		});
		it('it should return errors for new invalid timeentry', (done) => {
			const timeentry = {
				date: 'zzz',
				distance: '200',
				time: '600'
			};
			chai.request(app)
				.post('/timeentry')
				.set('Authorization', token)
				.send(timeentry)
				.end((err, res) => {
					res.should.have.status(400);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('errors');
					res.body.errors.should.be.a('object');
					res.body.errors.should.have.property('date');
					done();
				});
		});
	});

	describe('PUT /timeentry', () => {
		beforeEach((done) => { //Before each test we empty the database
			insTimeEntry(1, done);
		});
		it('it should update timeentry', (done) => {
			const timeentry = {
				user: '1',
				date: '123123123',
				distance: '200',
				time: '600'
			};
			chai.request(app)
				.put('/timeentry/1')
				.set('Authorization', token)
				.send(timeentry)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('timeentry');
					res.body.timeentry.should.have.property('id');
					res.body.timeentry.id.should.be.above(0);
					done();
				});
		});
		it('it should return errors for updating with invalid params', (done) => {
			const timeentry = {
				time: 'hi'
			};
			chai.request(app)
				.put('/timeentry/1')
				.set('Authorization', token)
				.send(timeentry)
				.end((err, res) => {
					res.should.have.status(400);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('errors');
					res.body.errors.should.be.a('object');
					res.body.errors.should.have.property('distance');
					res.body.errors.should.have.property('date');
					res.body.errors.should.have.property('time');
					done();
				});
		});
	});

	describe('DELETE /timeentry', () => {
		beforeEach((done) => { //Before each test we empty the database
			insTimeEntry(1, done);
		});
		it('it should delete timeentry', (done) => {
			chai.request(app)
				.delete('/timeentry/1')
				.set('Authorization', token)
				.end((err, res) => {
					should.not.exist(err);
					res.should.have.status(200);
					res.should.be.json;
					res.body.should.be.a('object');
					res.body.should.have.property('message');
					done();
				});
		});
	});

});


