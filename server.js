const express = require('express');
const bodyParser = require('body-parser');

const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const LocalStrategy = require('passport-local').Strategy;

const controllers = require('./controllers');
const util = require('./util');
const db = require('./db');
const config = require('./config');

const app = express();
app.set('port', (process.env.PORT || 3001));

util.initDB(db);

// Default user
if (process.env.NODE_ENV !== 'test') {
	db.get("SELECT * FROM user WHERE role = 'admin'", [], function (err, row) {
		if(!row) {
			console.log('Not found any user with admin role. Creating default admin with login: test@test.com and password: test.');
			db.run("INSERT INTO user (role, email, name, password) VALUES (?,?,?,?)", ['admin','test@test.com','Default admin', 'test'],
				function(err) { if(err) console.log('Failed to create default user:', err) });
		}
	});
}
// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
	app.use(express.static('build'));
}

// Setting up local login strategy
const localLogin = new LocalStrategy((username, password, done) => {
	db.get("SELECT * FROM user WHERE email = ?", [username], function (err, row) {
		if (err) { return done(err); }
		if (!row) { return done(null, false); }
		if (row.password !== password) { return done(null, false); }
		return done(null, row);
	});
});

const jwtLogin = new JwtStrategy(config.jwtOptions, (payload, done) => {
	db.get("SELECT * FROM user WHERE id = ?", [payload._id], function (err, row) {
		if (err) { return done({ errors: { main: err } }); }
		if (row) {
			done(null, row);
		} else {
			done(null, false);
		}
	});
});
passport.use(jwtLogin);
passport.use(localLogin);


// Auth middleware, disable in test environment
const requireAuth = passport.authenticate('jwt', { session: false });

const requireLogin = passport.authenticate('local', { session: false });


app.use(bodyParser.json());

app.all('/*', function(req, res, next) {
	// CORS headers
	res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	//res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
	res.header("Access-Control-Allow-Credentials", "true");
 	if (req.method == 'OPTIONS') {
		res.status(200).end();
	} else {
		next();
	}
});

// App routes
app.post('/register', controllers.auth.register);
app.post('/login', requireLogin, controllers.auth.login);
app.post('/logout', requireAuth, controllers.auth.logout);

app.get('/timeentry', requireAuth, controllers.timeentry.all);
app.post('/timeentry', requireAuth,  controllers.timeentry.add);
app.put('/timeentry/:id', requireAuth,  controllers.timeentry.update);
app.delete('/timeentry/:id', requireAuth,  controllers.timeentry.remove);

app.get('/user', requireAuth,  controllers.user.all);
app.post('/user', requireAuth,  controllers.user.add);
app.put('/user/:id', requireAuth,  controllers.user.update);
app.delete('/user/:id', requireAuth,  controllers.user.remove);


if (process.env.NODE_ENV === 'test') {
	// Giveaway errors to client
	app.use(function (err, req, res, next) {
		console.error(err.stack);
		res.status(500).send(err);
	});
}


app.listen(app.get('port'), () => {
	console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});

module.exports.app = app;
module.exports.db = db;
